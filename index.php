
<?php
/* Exercice 1 Créer une variable et l'initialiser à 0. Tant que cette variable n'atteint pas 10 :

l'afficher
incrémenter de 1
*/
/*$myVariable = 0;
$num =1;

while( $myVariable < 10) {
$num++;
$myVariable++;
}
         
         echo ("Ma variable est de $myVariable" );*/
         

/*Exercice 2 Créer deux variables. Initialiser la première à 0 et la deuxième avec un nombre compris en 1 et 100. 
Tant que la première variable n'est pas supérieur à 20 :

    multiplier la première variable avec la deuxième
    afficher le résultat
    incrémenter la première variable*/

/*$variableZero = 0;
$i = rand(1,100);
echo "i = $i<br />";

while ($variableZero <= 20) {
    echo $variableZero*$i;
    echo "<br />";
    $variableZero++;
}*/

/*Exercice 3 Créer deux variables. Initialiser la première à 100 et la deuxième avec un nombre compris en 1 et 100.
Tant que la première variable n'est pas inférieur ou égale à 10 :

    multiplier la première variable avec la deuxième
    afficher le résultat
    décrémenter la première variable*/

/*$myVariable = 100;
$i = rand(1,100);
echo "i = $i<br/>";

while ($myVariable >= 10) {
    echo $myVariable*$i;
    echo "<br />";
    $myVariable--;
}*/


/*Exercice 4 Créer une variable et l'initialiser à 1. Tant que cette variable n'atteint pas 10 :

    l'afficher
    l'incrementer de la moitié de sa valeur*/

/*$myVariable = 1;

while ($myVariable < 10) {
    echo "$myVariable <br/>";
    $myVariable += ($myVariable/2);
}*/

//Exercice 5 En allant de 1 à 15 avec un pas de 1, afficher le message On y arrive presque...

/*$myVariable = 1;
while ($myVariable <= 15) {
    echo "$myVariable.on y arrive presque...<br/>";
    $myVariable ++;
}*/

//Exercice 6 En allant de 20 à 0 avec un pas de 1, afficher le message C'est presque bon...

/*$myVariable = 20;
while ($myVariable >= 0) {
    echo "$myVariable.c'est presque bon...<br/>";
    $myVariable --;
}*/

//Exercice 7 En allant de 1 à 100 avec un pas de 15, afficher le message On tient le bon bout...

/*$myVariable = 1;
while ($myVariable <= 100) {
    echo "$myVariable.on tient le bon bout...<br/>";
    $myVariable += 15;
}*/

//Exercice 8 En allant de 200 à 0 avec un pas de 12, afficher le message Enfin ! ! !

$myVariable = 200;
while ($myVariable >= 0) {
    echo "$myVariable <br/>";
    $myVariable -= 12;
}

?>